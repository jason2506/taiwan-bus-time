// eslint-disable-next-line @typescript-eslint/no-var-requires
const prettierConfig = require('./prettier.config.cjs')

const singleQuote = prettierConfig.singleQuote
const project = ['tsconfig.json', 'tsconfig.*.json']

module.exports = {
  root: true,
  extends: [
    'eslint:recommended',
    'plugin:security/recommended-legacy',
    'plugin:import/recommended',
    'plugin:promise/recommended',
    'plugin:eslint-comments/recommended',

    'plugin:@typescript-eslint/strict-type-checked',
    'plugin:@typescript-eslint/stylistic-type-checked',
    'plugin:import/typescript',
    'standard-with-typescript',

    'plugin:solid/typescript',

    'plugin:prettier/recommended',
  ],
  plugins: ['simple-import-sort', 'unicorn'],
  settings: {
    'import/resolver': {
      typescript: {
        alwaysTryTypes: true,
        project,
      },
    },
  },
  parserOptions: {
    project,
  },
  rules: {
    // bulit-in rules
    'func-style': ['error', 'declaration'],
    'no-console':
      process.env['NODE_ENV'] === 'production' ? ['error', { allow: ['warn', 'error'] }] : 'warn',
    'no-debugger': process.env['NODE_ENV'] === 'production' ? 'error' : 'warn',
    'no-implicit-coercion': 'error',
    'no-negated-condition': 'off', // already handled by unicorn/no-negated-condition
    'no-tabs': ['error', { allowIndentationTabs: true }],
    'prefer-arrow-callback': 'error',

    // rules from @typescript-eslint/eslint-plugin
    '@typescript-eslint/class-literal-property-style': ['error', 'getters'],
    '@typescript-eslint/explicit-member-accessibility': 'error',
    '@typescript-eslint/member-ordering': [
      'error',
      {
        default: ['signature', 'field', 'constructor', ['get', 'set'], 'method'],
      },
    ],
    '@typescript-eslint/naming-convention': [
      'error',
      {
        selector: 'default',
        format: ['camelCase'],
      },
      {
        selector: 'import',
        format: ['camelCase', 'PascalCase'],
      },
      {
        selector: 'variable',
        format: ['camelCase', 'UPPER_CASE'],
        leadingUnderscore: 'allow',
      },
      {
        selector: 'function',
        format: ['camelCase', 'PascalCase'],
      },
      {
        selector: 'parameter',
        format: ['camelCase'],
        leadingUnderscore: 'allow',
      },
      {
        selector: 'memberLike',
        modifiers: ['private'],
        format: ['camelCase'],
        leadingUnderscore: 'require',
      },
      {
        selector: 'memberLike',
        modifiers: ['protected'],
        format: ['camelCase'],
        leadingUnderscore: 'require',
      },
      {
        selector: 'typeLike',
        format: ['PascalCase'],
      },
      {
        selector: 'enumMember',
        format: ['PascalCase'],
      },
      {
        selector: 'objectLiteralProperty',
        format: null,
      },
    ],
    '@typescript-eslint/no-dupe-class-members': 'off', // already handled by TypeScript
    '@typescript-eslint/no-redeclare': 'off', // already handled by TypeScript
    '@typescript-eslint/no-unnecessary-qualifier': 'error',
    '@typescript-eslint/no-unsafe-unary-minus': 'error',
    '@typescript-eslint/no-unused-vars': 'off', // already handled by TypeScript
    '@typescript-eslint/no-use-before-define': 'off', // already handled by TypeScript
    '@typescript-eslint/no-useless-empty-export': 'error',
    '@typescript-eslint/parameter-properties': 'error',
    '@typescript-eslint/prefer-enum-initializers': 'error',
    '@typescript-eslint/prefer-regexp-exec': 'error',
    '@typescript-eslint/quotes': [
      'error',
      singleQuote ? 'single' : 'double',
      { avoidEscape: true, allowTemplateLiterals: false },
    ],

    '@typescript-eslint/switch-exhaustiveness-check': 'error',

    // rules from eslint-plugin-eslint-comments
    'eslint-comments/no-unused-disable': 'error',

    // rules from eslint-plugin-import
    'import/consistent-type-specifier-style': 'error',
    'import/extensions': ['error', 'ignorePackages', { ts: 'never', tsx: 'never' }],
    'import/newline-after-import': 'error',
    'import/no-absolute-path': 'error',
    'import/no-cycle': 'error',
    'import/no-deprecated': 'error',
    'import/no-empty-named-blocks': 'error',
    'import/no-extraneous-dependencies': [
      'error',
      {
        devDependencies: ['vite.config.ts', 'src/sw.ts'],
        optionalDependencies: false,
        peerDependencies: false,
        bundledDependencies: false,
      },
    ],
    'import/no-mutable-exports': 'error',

    // rules from eslint-plugin-prettier
    'prettier/prettier': ['error', { singleQuote }],

    // rules from eslint-plugin-n
    'n/no-sync': 'error',
    'n/prefer-promises/dns': 'error',
    'n/prefer-promises/fs': 'error',

    // rules from eslint-plugin-promise
    'promise/no-multiple-resolved': 'error',
    'promise/prefer-await-to-then': 'error',

    // rules from eslint-plugin-security
    'security/detect-object-injection': 'off',

    // rules from eslint-plugin-simple-import-sort
    'simple-import-sort/imports': 'error',
    'simple-import-sort/exports': 'error',

    // rules from eslint-plugin-unicorn
    'unicorn/catch-error-name': 'error',
    'unicorn/consistent-destructuring': 'error',
    'unicorn/consistent-function-scoping': 'error',
    'unicorn/escape-case': 'error',
    'unicorn/expiring-todo-comments': 'error',
    'unicorn/filename-case': 'error',
    'unicorn/no-await-expression-member': 'error',
    'unicorn/no-empty-file': 'error',
    'unicorn/no-for-loop': 'error',
    'unicorn/no-hex-escape': 'error',
    'unicorn/no-lonely-if': 'error',
    'unicorn/no-negated-condition': 'error',
    'unicorn/no-object-as-default-parameter': 'error',
    'unicorn/no-static-only-class': 'error',
    'unicorn/no-this-assignment': 'error',
    'unicorn/no-typeof-undefined': 'error',
    'unicorn/no-unreadable-array-destructuring': 'error',
    'unicorn/no-unreadable-iife': 'error',
    'unicorn/no-useless-fallback-in-spread': 'error',
    'unicorn/no-useless-switch-case': 'error',
    'unicorn/no-useless-undefined': 'error',
    'unicorn/no-zero-fractions': 'error',
    'unicorn/number-literal-case': 'error',
    'unicorn/numeric-separators-style': 'error',
    'unicorn/prefer-default-parameters': 'error',
    'unicorn/prefer-export-from': 'error',
    'unicorn/prefer-logical-operator-over-ternary': 'error',
    'unicorn/prefer-node-protocol': 'error',
    'unicorn/prefer-switch': 'error',
    'unicorn/prefer-ternary': 'error',
    'unicorn/switch-case-braces': 'error',
    'unicorn/template-indent': 'error',
    'unicorn/text-encoding-identifier-case': 'error',
  },
  overrides: [
    {
      files: ['*.tsx'],
      rules: {
        'unicorn/filename-case': ['error', { case: 'pascalCase' }],
      },
    },
    {
      files: ['*.cjs'],
      extends: ['plugin:@typescript-eslint/disable-type-checked'],
    },
    {
      files: ['*.json'],
      rules: {
        '@typescript-eslint/no-unused-expressions': 'off',
        '@typescript-eslint/quotes': 'off',
      },
    },
  ],
}
