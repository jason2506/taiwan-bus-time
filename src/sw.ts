import { CacheableResponsePlugin } from 'workbox-cacheable-response'
import type { WorkboxPlugin } from 'workbox-core'
import { clientsClaim } from 'workbox-core'
import { ExpirationPlugin } from 'workbox-expiration'
import { cleanupOutdatedCaches, precacheAndRoute } from 'workbox-precaching'
import { registerRoute } from 'workbox-routing'
import { CacheFirst } from 'workbox-strategies'

declare let self: ServiceWorkerGlobalScope

cleanupOutdatedCaches()

precacheAndRoute(self.__WB_MANIFEST)

registerRoute(
  ({ url }) => url.origin === 'https://fonts.googleapis.com',
  new CacheFirst({
    cacheName: 'google-fonts',
    plugins: [
      new ExpirationPlugin({
        maxAgeSeconds: 30 * 60 * 60 * 24, // 30 days
        maxEntries: 10,
      }) as WorkboxPlugin,
      new CacheableResponsePlugin({
        statuses: [0, 200],
      }) as WorkboxPlugin,
    ],
  }),
)

registerRoute(
  ({ url }) => url.origin === 'https://fonts.gstatic.com',
  new CacheFirst({
    cacheName: 'gstatic-fonts',
    plugins: [
      new ExpirationPlugin({
        maxAgeSeconds: 30 * 60 * 60 * 24, // 30 days
        maxEntries: 10,
      }) as WorkboxPlugin,
      new CacheableResponsePlugin({
        statuses: [0, 200],
      }) as WorkboxPlugin,
    ],
  }),
)

registerRoute(
  ({ url }) =>
    url.origin === 'https://tcgbusfs.blob.core.windows.net' &&
    !url.pathname.endsWith('GetEstimateTime.gz'),
  new CacheFirst({
    cacheName: 'data-taipei-api',
    plugins: [
      new ExpirationPlugin({
        maxAgeSeconds: 60 * 60 * 24, // 1 day
        maxEntries: 50,
      }) as WorkboxPlugin,
      new CacheableResponsePlugin({
        statuses: [0, 200],
      }) as WorkboxPlugin,
    ],
  }),
)

void self.skipWaiting()
clientsClaim()
