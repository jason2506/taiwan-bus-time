/* @refresh reload */
import { Router } from '@solidjs/router'
import { render } from 'solid-js/web'

import { App } from './App'
import { routes } from './pages'
import { preventDefaultEvent } from './utils/events'

const root = document.getElementById('root')
if (root === null) {
  throw new Error('root === null')
}

window.addEventListener('contextmenu', preventDefaultEvent)

render(
  () => (
    <Router root={App} base={import.meta.env.BASE_URL}>
      {routes}
    </Router>
  ),
  root,
)
