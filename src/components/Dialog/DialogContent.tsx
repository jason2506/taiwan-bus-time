import { mergeRefs } from '@solid-primitives/refs'
import classNames from 'classnames'
import type { JSX } from 'solid-js'
import { createUniqueId, splitProps } from 'solid-js'

import { useDialogContext } from './DialogContext'
import styles from './style.module.scss'

export type DialogContentProps = Omit<JSX.FormHTMLAttributes<HTMLFormElement>, 'slot'>

export function DialogContent(props: DialogContentProps): JSX.Element {
  const [localProps, restProps] = splitProps(props, ['class', 'id', 'method', 'ref'])
  const { setForm } = useDialogContext()
  const defaultId = `dialog-form-${createUniqueId()}`
  return (
    <form
      {...restProps}
      id={localProps.id ?? defaultId}
      class={classNames(localProps.class, styles['content'])}
      method={localProps.method ?? 'dialog'}
      ref={mergeRefs(localProps.ref, setForm)}
      slot="content"
    />
  )
}
