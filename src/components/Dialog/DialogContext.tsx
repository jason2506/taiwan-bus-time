import type { ContextProviderProps } from '@solid-primitives/context'
import { createContextProvider } from '@solid-primitives/context'
import { createSignal } from 'solid-js'

export interface DialogContext {
  readonly form: () => HTMLFormElement | undefined
  readonly setForm: (form: HTMLFormElement) => void
}

export interface DialogContextProviderProps extends ContextProviderProps {}

// eslint-disable-next-line @typescript-eslint/naming-convention
const [DialogContextProvider, _useDialogContext] = createContextProvider<
  DialogContext,
  DialogContextProviderProps
>(() => {
  const [form, setForm] = createSignal<HTMLFormElement>()
  return { form, setForm }
})

function useDialogContext(): DialogContext {
  const context = _useDialogContext()
  if (context === undefined) {
    throw new Error('DialogContext is not provided')
  }

  return context
}

export { DialogContextProvider, useDialogContext }
