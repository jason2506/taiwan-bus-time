import classNames from 'classnames'
import type { JSX } from 'solid-js'
import { splitProps } from 'solid-js'
import type { DynamicProps } from 'solid-js/web'
import { Dynamic } from 'solid-js/web'

import { Checkbox } from '../Checkbox'
import { Radio } from '../Radio'
import styles from './style.module.scss'

const typeMap = {
  checkbox: Checkbox,
  radio: Radio,
} as const

type TypeMap = typeof typeMap

export type DialogFieldProps<T extends keyof TypeMap> = Omit<
  DynamicProps<TypeMap[T]>,
  'component'
> & {
  readonly children?: JSX.Element | undefined
  readonly class?: string | undefined
  readonly label: string
  readonly type: T
}

export function DialogField<T extends keyof TypeMap>(props: DialogFieldProps<T>): JSX.Element {
  const [localProps, restProps] = splitProps(props, ['children', 'class', 'label', 'type'])

  let labelEl: HTMLLabelElement
  return (
    <label
      class={classNames(localProps.class, styles['field'])}
      ref={(el) => {
        labelEl = el
      }}
    >
      <Dynamic<TypeMap[T]>
        {...(restProps as unknown as DynamicProps<TypeMap[T]>)}
        ref={(el: HTMLElement) => {
          setTimeout(() => {
            el.shadowRoot?.querySelector('md-ripple')?.attach(labelEl)
          }, 0)
        }}
        component={typeMap[localProps.type]}
      />
      <span class={styles['field-label']}>{props.label}</span>
      {localProps.children}
    </label>
  )
}
