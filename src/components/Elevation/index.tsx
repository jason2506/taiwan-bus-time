import '@material/web/elevation/elevation'

import type { MdElevation } from '@material/web/elevation/elevation'
import type { JSX } from 'solid-js'

import type { CommonAttributes, CustomElementAttributes } from '@/utils/attributes'

declare module 'solid-js' {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace JSX {
    interface IntrinsicElements {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      'md-elevation': CustomElementAttributes<MdElevation>
    }
  }
}

export interface ElevationProps extends CommonAttributes<MdElevation> {}

export function Elevation(props: ElevationProps): JSX.Element {
  return <md-elevation {...props} />
}
