import '@material/web/iconbutton/icon-button'

import type { MdIconButton } from '@material/web/iconbutton/icon-button'
import type { JSX } from 'solid-js'
import { splitProps } from 'solid-js'
import type { DynamicProps } from 'solid-js/web'
import { Dynamic } from 'solid-js/web'

import type { CommonAttributes, CustomElementAttributes } from '@/utils/attributes'
import { excludeBooleanValue } from '@/utils/attributes'

import type { IconName } from '../Icon'
import { Icon } from '../Icon'

type MdIconButtonPropertyNames = 'ariaHasPopup' | 'disabled' | 'href' | 'type'

declare module 'solid-js' {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace JSX {
    interface IntrinsicElements {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      'md-icon-button': CustomElementAttributes<MdIconButton, MdIconButtonPropertyNames>
    }
  }
}

export type IconButtonType = 'button' | 'reset' | 'submit'

export type IconButtonVariant = 'standard'

type MdIconButtonTagName<T extends IconButtonVariant = IconButtonVariant> = T extends 'standard'
  ? 'md-icon-button'
  : `md-${T}-icon-button`

export interface IconButtonProps<T extends IconButtonVariant>
  extends CommonAttributes<HTMLElementTagNameMap[MdIconButtonTagName<T>], 'aria-haspopup'> {
  readonly disabled?: boolean | undefined
  readonly filledIcon?: boolean | undefined
  readonly href?: string | undefined
  readonly icon: IconName
  readonly type?: IconButtonType | undefined
  readonly variant?: T | undefined
}

export function IconButton<T extends IconButtonVariant = 'standard'>(
  props: IconButtonProps<T>,
): JSX.Element {
  const [localProps, restProps] = splitProps(props, [
    'aria-haspopup',
    'disabled',
    'filledIcon',
    'href',
    'icon',
    'type',
    'variant',
  ])

  return (
    <Dynamic
      {...(restProps as DynamicProps<MdIconButtonTagName>)}
      component={
        // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
        localProps.variant === undefined || localProps.variant === 'standard'
          ? 'md-icon-button'
          : // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
            (`md-${localProps.variant}-icon-button` as MdIconButtonTagName)
      }
      prop:ariaHasPopup={excludeBooleanValue(localProps['aria-haspopup'])}
      prop:disabled={localProps.disabled}
      prop:href={localProps.href}
      prop:type={localProps.type}
    >
      <Icon filled={localProps.filledIcon} name={localProps.icon} />
    </Dynamic>
  )
}
