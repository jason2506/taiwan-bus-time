import '@material/web/radio/radio'

import type { MdRadio } from '@material/web/radio/radio'
import type { JSX } from 'solid-js'
import { splitProps } from 'solid-js'

import type { CommonAttributes, CustomElementAttributes } from '@/utils/attributes'

type MdRadioPropertyNames = 'checked' | 'disabled' | 'name' | 'value'

declare module 'solid-js' {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace JSX {
    interface IntrinsicElements {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      'md-radio': CustomElementAttributes<MdRadio, MdRadioPropertyNames>
    }
  }
}

export interface RadioProps extends CommonAttributes<MdRadio> {
  readonly checked?: boolean | undefined
  readonly disabled?: boolean | undefined
  readonly name?: string | undefined
  readonly value?: string | undefined
}

export function Radio(props: RadioProps): JSX.Element {
  const [localProps, restProps] = splitProps(props, ['checked', 'disabled', 'name', 'value'])
  return (
    <md-radio
      {...restProps}
      attr:touch-target="wrapper"
      prop:checked={localProps.checked}
      prop:disabled={localProps.disabled}
      prop:name={localProps.name}
      prop:value={localProps.value}
    />
  )
}
