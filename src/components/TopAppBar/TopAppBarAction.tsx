import classNames from 'classnames'
import type { JSX } from 'solid-js'
import { splitProps } from 'solid-js'

import type { IconButtonProps } from '../IconButton'
import { IconButton } from '../IconButton'
import styles from './style.module.scss'

export type TopAppBarActionProps = Omit<IconButtonProps<'standard'>, 'variant'>

export function TopAppBarAction(props: TopAppBarActionProps): JSX.Element {
  const [localProps, restProps] = splitProps(props, ['class'])
  return (
    <IconButton
      {...restProps}
      variant="standard"
      class={classNames(localProps.class, styles['icon-button'])}
    />
  )
}
