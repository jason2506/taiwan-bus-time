import classNames from 'classnames'
import type { JSX } from 'solid-js'
import { splitProps } from 'solid-js'

import type { IconButtonProps } from '../IconButton'
import { IconButton } from '../IconButton'
import styles from './style.module.scss'

export interface TopAppBarNavigationProps extends IconButtonProps<'standard'> {}

export function TopAppBarNavigation(props: TopAppBarNavigationProps): JSX.Element {
  const [localProps, restProps] = splitProps(props, ['class'])
  return <IconButton {...restProps} class={classNames(localProps.class, styles['icon-button'])} />
}
