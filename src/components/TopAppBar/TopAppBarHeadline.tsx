import classNames from 'classnames'
import type { JSX, ValidComponent } from 'solid-js'
import { splitProps } from 'solid-js'
import type { DynamicProps } from 'solid-js/web'
import { Dynamic } from 'solid-js/web'

import styles from './style.module.scss'

const defaultComponent = 'h1'

export type TopAppBarHeadlineProps<T extends ValidComponent = typeof defaultComponent> = Omit<
  DynamicProps<T>,
  'component' | 'slot'
> & {
  readonly as?: T | undefined
  readonly class?: string | undefined
}

export function TopAppBarHeadline<T extends ValidComponent = typeof defaultComponent>(
  props: TopAppBarHeadlineProps<T>,
): JSX.Element {
  const [localProps, restProps] = splitProps(props, ['as', 'class'])
  return (
    <Dynamic
      {...restProps}
      component={localProps.as ?? defaultComponent}
      class={classNames(localProps.class, styles['headline'])}
    />
  )
}
