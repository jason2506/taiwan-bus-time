import type { JSX } from 'solid-js'
import { createEffect, createSignal } from 'solid-js'

import type { FontSize, Theme } from '@/services'
import { usePreferenceService } from '@/services'

export interface ThemeProviderProps {
  readonly children?: JSX.Element
}

export function ThemeProvider(props: ThemeProviderProps): JSX.Element {
  const preferences = usePreferenceService()
  const preferDarkTheme = window.matchMedia('(prefers-color-scheme: dark)')
  const [isSystemDarkTheme, setSystemDarkTheme] = createSignal(preferDarkTheme.matches)
  preferDarkTheme.addEventListener('change', (event) => {
    setSystemDarkTheme(event.matches)
  })

  const root = document.documentElement
  createEffect<Exclude<Theme, 'system'>>((prevTheme) => {
    const preferenceTheme = preferences.get('theme')
    const theme =
      preferenceTheme === 'system' ? (isSystemDarkTheme() ? 'dark' : 'light') : preferenceTheme
    if (prevTheme !== undefined) {
      root.classList.remove(`${prevTheme}-theme`)
    }

    root.classList.add(`${theme}-theme`)
    return theme
  })
  createEffect<FontSize>((prevFontSize) => {
    const fontSize = preferences.get('fontSize')
    if (prevFontSize !== undefined) {
      root.classList.remove(`text-${prevFontSize}`)
    }

    root.classList.add(`text-${fontSize}`)
    return fontSize
  })
  return <>{props.children}</>
}
