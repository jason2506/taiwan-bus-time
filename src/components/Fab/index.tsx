import '@material/web/fab/fab'

import type { MdFab } from '@material/web/fab/fab'
import type { JSX } from 'solid-js'
import { splitProps } from 'solid-js'

import type { CommonAttributes, CustomElementAttributes } from '@/utils/attributes'

import type { IconName } from '../Icon'
import { Icon } from '../Icon'

type MdFabPropertyNames = 'label' | 'size' | 'variant'

declare module 'solid-js' {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace JSX {
    interface IntrinsicElements {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      'md-fab': CustomElementAttributes<MdFab, MdFabPropertyNames>
    }
  }
}

export type FabSize = MdFab['size']

export type FabVariant = MdFab['variant']

export interface FabProps extends CommonAttributes<MdFab> {
  readonly icon?: IconName | undefined
  readonly label?: string | undefined
  readonly size?: FabSize | undefined
  readonly variant?: FabVariant | undefined
}

export function Fab(props: FabProps): JSX.Element {
  const [localProps, restProps] = splitProps(props, ['icon', 'label', 'size', 'variant'])
  return (
    <md-fab
      {...restProps}
      prop:label={localProps.label}
      prop:size={localProps.size}
      prop:variant={localProps.variant}
    >
      {localProps.icon !== undefined && <Icon slot="icon" name={localProps.icon} />}
    </md-fab>
  )
}
