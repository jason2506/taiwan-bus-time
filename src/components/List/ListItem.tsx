import '@material/web/list/list-item'

import type { MdListItem } from '@material/web/list/list-item'
import type { JSX } from 'solid-js'
import { splitProps } from 'solid-js'

import type { CommonAttributes, CustomElementAttributes } from '@/utils/attributes'
import { excludeBooleanValue } from '@/utils/attributes'

type MdListItemPropertyNames = 'ariaHasPopup' | 'href' | 'target' | 'type'

declare module 'solid-js' {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace JSX {
    interface IntrinsicElements {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      'md-list-item': CustomElementAttributes<MdListItem, MdListItemPropertyNames>
    }
  }
}

export type ListItemTarget = MdListItem['target']

export type ListItemType = MdListItem['type']

export interface ListItemProps extends CommonAttributes<MdListItem, 'aria-haspopup'> {
  readonly href?: string | undefined
  readonly target?: ListItemTarget | undefined
  readonly type?: ListItemType | undefined
}

export function ListItem(props: ListItemProps): JSX.Element {
  const [localProps, restProps] = splitProps(props, ['aria-haspopup', 'href', 'target', 'type'])
  return (
    <md-list-item
      {...restProps}
      prop:ariaHasPopup={excludeBooleanValue(localProps['aria-haspopup'])}
      prop:href={localProps.href}
      prop:target={localProps.target}
      prop:type={localProps.type}
    />
  )
}
