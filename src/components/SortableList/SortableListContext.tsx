import type { ContextProviderProps } from '@solid-primitives/context'
import { createContextProvider } from '@solid-primitives/context'

import type { JSXEvent } from '@/utils/events'

export interface SortableListContext {
  readonly handleDragStart: (index: number, event: JSXEvent<HTMLElement, PointerEvent>) => void
}

export interface SortableListContextProviderProps
  extends SortableListContext,
    ContextProviderProps {}

// eslint-disable-next-line @typescript-eslint/naming-convention
const [SortableListContextProvider, _useSortableListContext] = createContextProvider<
  SortableListContext,
  SortableListContextProviderProps
>((props: SortableListContextProviderProps) => props)

function useSortableListContext(): SortableListContext {
  const context = _useSortableListContext()
  if (context === undefined) {
    throw new Error('SortableListContext is not provided')
  }

  return context
}

export { SortableListContextProvider, useSortableListContext }
