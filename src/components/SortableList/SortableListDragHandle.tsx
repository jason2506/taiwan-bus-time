import '@material/web/list/list-item'

import classNames from 'classnames'
import type { JSX } from 'solid-js'
import { splitProps } from 'solid-js'

import { preventDefaultEvent } from '@/utils/events'

import type { IconButtonProps } from '../IconButton'
import { IconButton } from '../IconButton'
import { useSortableListContext } from './SortableListContext'
import styles from './style.module.scss'

export interface SortableListDragHandleProps
  extends Omit<
    IconButtonProps<'standard'>,
    'icon' | 'onContextMenu' | 'onClick' | 'onPointerDown'
  > {
  readonly index: number
}

export function SortableListDragHandle(props: SortableListDragHandleProps): JSX.Element {
  const [localProps, restProps] = splitProps(props, ['class', 'index'])
  const { handleDragStart } = useSortableListContext()
  return (
    <IconButton
      {...restProps}
      icon="drag_handle"
      class={classNames(localProps.class, styles['draggable-handle'])}
      onContextMenu={preventDefaultEvent}
      onClick={preventDefaultEvent}
      onPointerDown={[handleDragStart, localProps.index]}
    />
  )
}
