import type { MdList } from '@material/web/list/list'
import type { MdListItem } from '@material/web/list/list-item'
import { mergeRefs } from '@solid-primitives/refs'
import type { JSX } from 'solid-js'
import { onCleanup, splitProps } from 'solid-js'

import type { JSXEvent } from '@/utils/events'

import type { ListProps } from '../List'
import { List } from '../List'
import { SortableListContextProvider } from './SortableListContext'
import styles from './style.module.scss'

const draggingClassName = styles['dragging'] ?? ''
const droppingClassName = styles['dropping'] ?? ''
const draggableClassName = styles['draggable'] ?? ''
const draggedClassName = styles['dragged'] ?? ''
const scrollThreshold = 50

interface Dragged {
  readonly el: HTMLElement
  readonly index: number
  readonly offset: number
}

export interface SortableListProps extends ListProps {
  readonly container: HTMLElement
  readonly onDropped: (from: number, to: number) => void
}

export function SortableList(props: SortableListProps): JSX.Element {
  const [localProps, restProps] = splitProps(props, ['ref'])
  let listEl: MdList
  let dragged: Dragged | undefined
  let droppedIndex = 0
  let scrollingSpeed = 0
  let previousScrollTimeStamp: number | undefined

  function getDraggables(): NodeListOf<MdListItem> {
    return listEl.querySelectorAll<MdListItem>(`.${draggableClassName}`)
  }

  function handleDragStart(index: number, event: JSXEvent<HTMLElement, PointerEvent>): void {
    if (dragged !== undefined) {
      return
    }

    const el = event.currentTarget.closest<MdListItem>(`.${draggableClassName}`)
    if (el === null) {
      return
    }

    event.preventDefault()
    event.stopPropagation()

    listEl.classList.add(draggingClassName)
    el.classList.add(draggedClassName)
    dragged = {
      el,
      index,
      offset: event.clientY + props.container.scrollTop,
    }
    droppedIndex = index

    document.addEventListener('pointermove', handleDragMove, { passive: true })
    document.addEventListener('pointerup', handleDragEnd, { passive: true })
    document.addEventListener('pointercancel', handleDragEnd, { passive: true })
  }

  function handleCleanup(): void {
    scrollingSpeed = 0
    document.removeEventListener('pointermove', handleDragMove)
    document.removeEventListener('pointerup', handleDragEnd)
    document.removeEventListener('pointercancel', handleDragEnd)
  }

  function handleDragEnd(): void {
    if (dragged === undefined) {
      return
    }

    handleCleanup()

    const draggedIndex = dragged.index
    const droppedOffset =
      listEl.querySelector<MdListItem>(`:nth-child(${droppedIndex + 1} of .${draggableClassName})`)
        ?.offsetTop ?? 0

    const draggedEl = dragged.el
    draggedEl.style.transform = `translateY(${droppedOffset - draggedEl.offsetTop}px)`
    listEl.classList.remove(draggingClassName)
    listEl.classList.add(droppingClassName)
    setTimeout(() => {
      draggedEl.classList.remove(draggedClassName)
      listEl.classList.remove(droppingClassName)
      for (const draggable of getDraggables()) {
        draggable.style.transform = ''
      }

      props.onDropped(draggedIndex, droppedIndex)
      dragged = undefined
    }, 300)
  }

  function handleDragMove(event: PointerEvent): void {
    if (dragged === undefined) {
      return
    }

    const container = props.container
    const clientY = event.clientY
    const draggedEl = dragged.el
    const startOffset = dragged.offset
    const offset = clientY + container.scrollTop
    const listOffset = listEl.offsetTop
    const placeholdHeight = draggedEl.offsetHeight

    const minOffset = -draggedEl.offsetTop
    const maxOffset = minOffset + listEl.offsetHeight - placeholdHeight
    const moveOffset = Math.max(Math.min(offset - startOffset, maxOffset), minOffset)
    draggedEl.style.transform = `translateY(${moveOffset}px)`

    droppedIndex = dragged.index
    let transform: string
    let needTransform: (offsetTop: number, offsetHeight: number) => boolean
    if (moveOffset <= 0) {
      transform = `translateY(${placeholdHeight}px)`
      needTransform = (offsetTop, offsetHeight) => {
        const bottom = listOffset + offsetTop + offsetHeight
        const result = bottom < startOffset && bottom > offset
        if (result) --droppedIndex
        return result
      }
    } else {
      transform = `translateY(-${placeholdHeight}px)`
      needTransform = (offsetTop) => {
        const top = listOffset + offsetTop
        const result = top > startOffset && top < offset
        if (result) ++droppedIndex
        return result
      }
    }

    for (const draggable of getDraggables()) {
      if (draggable === draggedEl) {
        continue
      }

      const { offsetTop, offsetHeight, style } = draggable
      style.transform = needTransform(offsetTop, offsetHeight) ? transform : ''
    }

    const top = clientY - container.offsetTop
    const bottom = container.offsetHeight - top
    if (top < scrollThreshold) {
      scrollingSpeed = top - scrollThreshold
    } else if (bottom < scrollThreshold) {
      scrollingSpeed = scrollThreshold - bottom
    } else {
      scrollingSpeed = 0
    }

    if (previousScrollTimeStamp === undefined) {
      window.requestAnimationFrame(handleDragScroll)
    }
  }

  function handleDragScroll(timeStamp: number): void {
    const elapsed = previousScrollTimeStamp === undefined ? 0 : timeStamp - previousScrollTimeStamp
    props.container.scrollTop += (elapsed * scrollingSpeed) / scrollThreshold
    if (scrollingSpeed === 0) {
      previousScrollTimeStamp = undefined
    } else {
      previousScrollTimeStamp = timeStamp
      window.requestAnimationFrame(handleDragScroll)
    }
  }

  onCleanup(handleCleanup)
  return (
    <SortableListContextProvider handleDragStart={handleDragStart}>
      <List
        {...restProps}
        ref={mergeRefs(localProps.ref, (el) => {
          listEl = el
        })}
      />
    </SortableListContextProvider>
  )
}
