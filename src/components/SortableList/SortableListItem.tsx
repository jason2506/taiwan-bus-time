import '@material/web/list/list-item'

import classNames from 'classnames'
import type { JSX } from 'solid-js'
import { splitProps } from 'solid-js'

import { preventDefaultEvent } from '@/utils/events'

import type { ListItemProps } from '../List'
import { ListItem } from '../List'
import styles from './style.module.scss'

export interface SortableListItemProps extends Omit<ListItemProps, 'onDragStart'> {}

export function SortableListItem(props: SortableListItemProps): JSX.Element {
  const [localProps, restProps] = splitProps(props, ['class'])
  return (
    <ListItem
      {...restProps}
      class={classNames(localProps.class, styles['draggable'])}
      onDragStart={preventDefaultEvent}
    />
  )
}
