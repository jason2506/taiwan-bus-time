import type { JSX } from 'solid-js'

import styles from './style.module.scss'

export interface LayoutProps {
  readonly children: JSX.Element
}

export function Layout(props: LayoutProps): JSX.Element {
  return <div class={styles['container']}>{props.children}</div>
}
