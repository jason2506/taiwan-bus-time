import '@material/web/button/text-button'

import type { MdTextButton } from '@material/web/button/text-button'
import type { JSX } from 'solid-js'
import { splitProps } from 'solid-js'
import type { DynamicProps } from 'solid-js/web'
import { Dynamic } from 'solid-js/web'

import type { CommonAttributes, CustomElementAttributes } from '@/utils/attributes'

import type { IconName } from '../Icon'
import { Icon } from '../Icon'

type MdButtonPropertyNames = 'disabled' | 'hasIcon' | 'type'

interface MdButtonAttrAttributes {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  readonly 'attr:form'?: string | undefined
  // eslint-disable-next-line @typescript-eslint/naming-convention
  readonly 'attr:value'?: string | undefined
}

declare module 'solid-js' {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace JSX {
    interface IntrinsicElements {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      'md-text-button': CustomElementAttributes<MdTextButton, MdButtonPropertyNames> &
        MdButtonAttrAttributes
    }
  }
}

export type ButtonType = 'button' | 'reset' | 'submit'

export type ButtonVariant = 'text'

type MdButtonTagName<T extends ButtonVariant = ButtonVariant> = `md-${T}-button`

export interface ButtonProps<T extends ButtonVariant>
  extends CommonAttributes<HTMLElementTagNameMap[MdButtonTagName<T>]> {
  readonly children: string
  readonly disabled?: boolean | undefined
  readonly form?: string | undefined
  readonly icon?: IconName | undefined
  readonly type?: ButtonType | undefined
  readonly value?: string | undefined
  readonly variant: T
}

export function Button<T extends ButtonVariant>(props: ButtonProps<T>): JSX.Element {
  const [localProps, restProps] = splitProps(props, [
    'children',
    'disabled',
    'form',
    'icon',
    'type',
    'value',
    'variant',
  ])
  return (
    <Dynamic
      {...(restProps as DynamicProps<MdButtonTagName>)}
      component={`md-${localProps.variant}-button` as MdButtonTagName}
      attr:form={localProps.form}
      attr:value={localProps.value}
      prop:disabled={localProps.disabled}
      prop:hasIcon={localProps.icon !== undefined}
      prop:type={localProps.type}
    >
      {localProps.icon !== undefined && <Icon slot="icon" name={localProps.icon} />}
      {localProps.children}
    </Dynamic>
  )
}
