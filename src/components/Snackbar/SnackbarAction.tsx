import type { MdTextButton } from '@material/web/button/text-button'
import classNames from 'classnames'
import type { JSX } from 'solid-js'
import { splitProps } from 'solid-js'

import type { JSXEvent } from '@/utils/events'

import type { ButtonProps } from '../Button'
import { Button } from '../Button'
import { useSnackbarContext } from './SnackbarContext'
import styles from './style.module.scss'

export interface SnackbarActionProps extends Omit<ButtonProps<'text'>, 'variant'> {}

export function SnackbarAction(props: SnackbarActionProps): JSX.Element {
  const [localProps, restProps] = splitProps(props, ['class', 'onClick'])

  const { close } = useSnackbarContext()
  function handleClick(event: JSXEvent<MdTextButton, MouseEvent>): void {
    const onClick = localProps.onClick
    if (typeof onClick === 'function') {
      onClick(event)
    } else if (onClick !== undefined) {
      onClick[0](onClick[1], event)
    }

    if (!event.defaultPrevented) {
      close()
    }
  }

  return (
    <Button
      {...restProps}
      variant="text"
      class={classNames(localProps.class, styles['action'])}
      onClick={handleClick}
    />
  )
}
