import classNames from 'classnames'
import type { JSX } from 'solid-js'
import { createEffect, createSignal, Show, splitProps } from 'solid-js'

import type { CommonAttributes } from '@/utils/attributes'

import { Elevation } from '../Elevation'
import { IconButton } from '../IconButton'
import { SnackbarContextProvider } from './SnackbarContext'
import styles from './style.module.scss'

export interface SnackbarProps extends CommonAttributes<HTMLDivElement> {
  readonly duration?: number | undefined
  readonly onClosed?: () => void
  readonly open?: boolean | undefined
  readonly showCloseIcon?: boolean | undefined
}

export function Snackbar(props: SnackbarProps): JSX.Element {
  const [localProps, restProps] = splitProps(props, [
    'children',
    'class',
    'duration',
    'onClosed',
    'open',
    'showCloseIcon',
  ])

  const [isOpen, setOpen] = createSignal(false)
  const [isClosing, setClosing] = createSignal(false)

  let openTimeoutId: number | undefined
  function open(): void {
    if (isOpen() || isClosing()) {
      return
    }

    setOpen(true)
    const duration = localProps.duration ?? 4000
    if (duration > 0) {
      openTimeoutId = setTimeout(close, duration)
    }
  }

  function close(): void {
    if (!isOpen() || isClosing()) {
      return
    }

    clearTimeout(openTimeoutId)
    openTimeoutId = undefined

    setClosing(true)
    setOpen(false)

    setTimeout(() => {
      setClosing(false)
      localProps.onClosed?.()
    }, 75)
  }

  createEffect(() => {
    if (localProps.open ?? false) {
      queueMicrotask(open)
    } else {
      queueMicrotask(close)
    }
  })

  return (
    <SnackbarContextProvider close={close}>
      <div
        {...restProps}
        aria-hidden={!isOpen()}
        role="status"
        class={classNames(
          localProps.class,
          styles['container'],
          isOpen() && !isClosing() && styles['open'],
          isClosing() && styles['closing'],
        )}
      >
        <Elevation />
        {localProps.children}
        <Show when={localProps.showCloseIcon}>
          <IconButton icon="close" class={styles['close-icon']} onClick={close} />
        </Show>
      </div>
    </SnackbarContextProvider>
  )
}
