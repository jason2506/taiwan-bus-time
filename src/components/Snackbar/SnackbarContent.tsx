import type { JSX } from 'solid-js'

import type { CommonAttributes } from '@/utils/attributes'

import styles from './style.module.scss'

export interface SnackbarContentProps extends CommonAttributes<HTMLElement> {}

export function SnackbarContent(props: SnackbarContentProps): JSX.Element {
  return <span class={styles['content']}>{props.children}</span>
}
