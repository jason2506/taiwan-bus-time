import '@material/web/menu/menu-item'

import type { MdMenuItem } from '@material/web/menu/menu-item'
import type { JSX } from 'solid-js'
import { splitProps } from 'solid-js'

import type { CommonAttributes, CustomElementAttributes } from '@/utils/attributes'

type MdMenuItemPropertyNames = 'href' | 'type'

declare module 'solid-js' {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace JSX {
    interface IntrinsicElements {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      'md-menu-item': CustomElementAttributes<MdMenuItem, MdMenuItemPropertyNames>
    }
  }
}

export type MenuItemType = MdMenuItem['type']

export interface MenuItemProps extends CommonAttributes<MdMenuItem> {
  readonly href?: string | undefined
  readonly type?: MenuItemType | undefined
}

export function MenuItem(props: MenuItemProps): JSX.Element {
  const [localProps, restProps] = splitProps(props, ['href', 'type'])
  return <md-menu-item {...restProps} prop:href={localProps.href} prop:type={localProps.type} />
}
