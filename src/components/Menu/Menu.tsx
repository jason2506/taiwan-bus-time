import '@material/web/menu/menu'

import type { MdMenu } from '@material/web/menu/menu'
import type { JSX } from 'solid-js'
import { splitProps } from 'solid-js'

import type { CommonAttributes, CustomElementAttributes, OnAttributes } from '@/utils/attributes'

type MdMenuPropertyNames =
  | 'anchorCorner'
  | 'anchorElement'
  | 'menuCorner'
  | 'open'
  | 'positioning'
  | 'skipRestoreFocus'

// eslint-disable-next-line @typescript-eslint/consistent-type-definitions
type MdMenuEvents = {
  readonly closed: Event
}

declare module 'solid-js' {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace JSX {
    interface IntrinsicElements {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      'md-menu': CustomElementAttributes<MdMenu, MdMenuPropertyNames, MdMenuEvents>
    }
  }
}

export type MenuPositioning = MdMenu['positioning']

export type MenuAnchorCorner = MdMenu['anchorCorner']

export type MenuCorner = MdMenu['menuCorner']

export type MenuDefaultFocus = MdMenu['defaultFocus']

export interface MenuProps extends CommonAttributes<MdMenu>, OnAttributes<MdMenu, MdMenuEvents> {
  readonly anchor?: HTMLElement | undefined
  readonly anchorCorner?: MenuAnchorCorner | undefined
  readonly menuCorner?: MenuCorner | undefined
  readonly open?: boolean | undefined
  readonly positioning?: MenuPositioning | undefined
}

export function Menu(props: MenuProps): JSX.Element {
  const [localProps, restProps] = splitProps(props, [
    'anchor',
    'anchorCorner',
    'menuCorner',
    'open',
    'positioning',
  ])

  return (
    <md-menu
      {...restProps}
      prop:anchorCorner={localProps.anchorCorner}
      prop:anchorElement={localProps.anchor}
      prop:menuCorner={localProps.menuCorner}
      prop:open={localProps.open}
      prop:positioning={localProps.positioning}
      prop:skipRestoreFocus={true}
    />
  )
}
