import type { MdDialog } from '@material/web/dialog/dialog'
import { Entries } from '@solid-primitives/keyed'
import type { JSX } from 'solid-js'

import {
  Dialog,
  DialogAction,
  DialogActions,
  DialogContent,
  DialogField,
  DialogHeadline,
} from '../Dialog'

export interface RadioGroupDialogProps {
  readonly headline: string
  readonly options: Readonly<Record<string, string>>
  readonly value?: string | undefined
  readonly onClose?: ((value: string) => void) | undefined
  readonly open?: boolean | undefined
}

export function RadioGroupDialog(props: RadioGroupDialogProps): JSX.Element {
  let dialogEl!: MdDialog
  function setCheckedValue(value: string): void {
    void dialogEl.close(value)
  }

  function onCloseDialog(): void {
    props.onClose?.(dialogEl.returnValue)
    dialogEl.returnValue = ''
  }

  return (
    <Dialog
      ref={(el) => {
        dialogEl = el
      }}
      open={props.open}
      on:closed={onCloseDialog}
    >
      <DialogHeadline>{props.headline}</DialogHeadline>
      <DialogContent>
        <Entries of={props.options}>
          {(value, label) => (
            <DialogField
              type="radio"
              label={label()}
              name="option"
              value={value}
              checked={value === props.value}
              onClick={[setCheckedValue, value]}
            />
          )}
        </Entries>
      </DialogContent>
      <DialogActions>
        <DialogAction>取消</DialogAction>
      </DialogActions>
    </Dialog>
  )
}
