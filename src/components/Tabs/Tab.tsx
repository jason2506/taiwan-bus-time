import '@material/web/tabs/primary-tab'

import type { MdPrimaryTab } from '@material/web/tabs/primary-tab'
import type { JSX } from 'solid-js'
import { splitProps } from 'solid-js'
import type { DynamicProps } from 'solid-js/web'
import { Dynamic } from 'solid-js/web'

import type { CommonAttributes, CustomElementAttributes } from '@/utils/attributes'

type MdTabPropertyNames = 'active' | 'selected' | 'tabIndex'

interface MdTabAttrAttributes {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  readonly 'attr:aria-controls'?: string | undefined
}

declare module 'solid-js' {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace JSX {
    interface IntrinsicElements {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      'md-primary-tab': CustomElementAttributes<MdPrimaryTab, MdTabPropertyNames> &
        MdTabAttrAttributes
    }
  }
}

export type TabVariant = 'primary'

type MdTabTagName<T extends TabVariant = TabVariant> = `md-${T}-tab`

export interface TabProps<T extends TabVariant>
  extends CommonAttributes<HTMLElementTagNameMap[MdTabTagName<T>], 'aria-controls'> {
  readonly active?: boolean | undefined
  readonly selected?: boolean | undefined
  readonly variant: T
}

export function Tab<T extends TabVariant>(props: TabProps<T>): JSX.Element {
  const [localProps, restProps] = splitProps(props, [
    'active',
    'aria-controls',
    'selected',
    'variant',
  ])
  return (
    <Dynamic
      {...(restProps as DynamicProps<MdTabTagName>)}
      component={`md-${localProps.variant}-tab` as MdTabTagName}
      attr:aria-controls={localProps['aria-controls']}
      prop:active={localProps.active}
      prop:selected={localProps.selected}
      prop:tabIndex={localProps.active === true ? 0 : -1}
    />
  )
}
