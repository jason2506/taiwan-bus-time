import type { RouteDefinition } from '@solidjs/router'

import BookmarkListPage from './BookmarkListPage'
import BusRouteDetailPage from './BusRouteDetailPage'
import BusRouteListPage from './BusRouteListPage'
import CategoryListPage from './CategoryListPage'
import ImportExportPage from './ImportExportPage'
import LicenseListPage from './LicenseListPage'
import PreferencePage from './PreferencePage'

export const routes: RouteDefinition[] = [
  { path: '/', component: BookmarkListPage },
  { path: '/categories', component: CategoryListPage },
  { path: '/routes', component: BusRouteListPage },
  { path: '/routes/:routeId', component: BusRouteDetailPage },
  { path: '/preferences', component: PreferencePage },
  {
    path: '/preferences/import-export',
    component: ImportExportPage,
  },
  { path: '/licenses', component: LicenseListPage },
]
