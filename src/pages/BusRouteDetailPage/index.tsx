import { useLocation, useNavigate, useParams } from '@solidjs/router'
import type { JSX } from 'solid-js'
import { createEffect, createMemo, createSignal, For, Show } from 'solid-js'

import { BookmarkDialog } from '@/components/BookmarkDialog'
import { BusArrivalTime } from '@/components/BusArrivalTime'
import { IconButton } from '@/components/IconButton'
import { ItemEnd, ItemHeadline, ItemStart } from '@/components/Item'
import { Layout, LayoutContent, LayoutHeader } from '@/components/Layout'
import { List, ListItem } from '@/components/List'
import { Snackbar, SnackbarContent } from '@/components/Snackbar'
import { Tab, Tabs } from '@/components/Tabs'
import {
  TopAppBar,
  TopAppBarAction,
  TopAppBarActions,
  TopAppBarHeadline,
  TopAppBarNavigation,
} from '@/components/TopAppBar'
import { createQueue } from '@/primitives'
import type { BusDirection } from '@/services'
import { useBusInfoService, useBusStopBookmarkService } from '@/services'

import styles from './style.module.scss'

const targetClassName = styles['target'] ?? ''
const hashPattern = /^#direction-(0|1)(-([A-Z]{3}\d+)$|$)/

function scrollToTarget(el: HTMLElement): void {
  const observer = new MutationObserver(() => {
    if (el.isConnected) {
      el.classList.add(targetClassName)
      setTimeout(() => {
        el.classList.remove(targetClassName)
        el.scrollIntoView({
          behavior: 'instant',
          block: 'center',
          inline: 'center',
        })
      }, 0)
      observer.disconnect()
    }
  })

  observer.observe(el, { attributes: true })
}

export default function BusRouteDetailPage(): JSX.Element {
  const params = useParams<{ readonly routeId: string }>()
  const routeId = createMemo(() => decodeURIComponent(params.routeId))

  const busInfoService = useBusInfoService()
  // eslint-disable-next-line solid/reactivity
  const route = busInfoService.getRoute(routeId)
  // eslint-disable-next-line solid/reactivity
  const stops = busInfoService.getStopsByRoute(routeId)
  // eslint-disable-next-line solid/reactivity
  const estimateTimes = busInfoService.getEstimateTimesByRoute(routeId)
  const isBidirection = createMemo(() => stops.data()[1].length > 0)

  const bookmarkService = useBusStopBookmarkService()
  const [bookmarkCategories, setBookmarkCategories] = createSignal<{
    readonly stopId: string
    readonly stopName: string
    readonly categories: Record<string, readonly [string, boolean]>
  } | null>(null)
  function openBookmarkDialog([stopId, stopName]: readonly [string, string]): void {
    const selectedCategoryIds = new Set(bookmarkService.getCategoriesByStop(stopId))
    const categoryStatus: Record<string, readonly [string, boolean]> = {}
    const categories = bookmarkService.getCategories()
    for (const [categoryId, categoryName] of Object.entries(categories)) {
      categoryStatus[categoryId] = [categoryName, selectedCategoryIds.has(categoryId)]
    }
    setBookmarkCategories({ stopId, stopName, categories: categoryStatus })
  }

  const navigate = useNavigate()
  const location = useLocation()
  const locationHash = createMemo<{
    readonly direction: BusDirection
    readonly stopId?: string | undefined
  }>(() => {
    const match = hashPattern.exec(location.hash)
    return match === null
      ? { direction: '0' }
      : { direction: match[1] as BusDirection, stopId: match[3] }
  })
  function setDirection(direction: BusDirection): void {
    navigate(`#direction-${direction}`, { replace: true })
  }

  const [snackbarText, { push: pushSnackbarText, pop: popSnackbarText }] = createQueue<string>()
  function onCloseBookmarkDialog(categories?: {
    readonly updated: Readonly<Record<string, boolean>>
    readonly created: readonly string[]
  }): void {
    if (categories !== undefined) {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const { stopId, stopName } = bookmarkCategories()!
      const isNewBookmark = !bookmarkService.hasBookmark(stopId)
      bookmarkService.updateBookmarks([stopId], categories.updated, categories.created)
      const hasBookmark = bookmarkService.hasBookmark(stopId)
      if (!isNewBookmark || hasBookmark) {
        pushSnackbarText(
          isNewBookmark
            ? `已將站牌「${stopName}」加入收藏。`
            : hasBookmark
              ? `已更新站牌「${stopName}」的收藏分類。`
              : `已將站牌「${stopName}」從收藏中移除。`,
        )
      }
    }
    setBookmarkCategories(null)
  }

  createEffect(() => {
    if (!route.isLoading() && route.error() !== undefined) {
      pushSnackbarText('無法取得公車路線資訊。')
    } else if (!stops.isLoading() && stops.error() !== undefined) {
      pushSnackbarText('無法取得公車站牌資訊。')
    } else if (!estimateTimes.isLoading() && estimateTimes.error() !== undefined) {
      pushSnackbarText('無法取得公車動態資訊。')
    }
  })

  return (
    <Layout>
      <LayoutHeader
        title={route.data()?.name}
        loadingInfo={
          route.isLoading() || stops.isLoading() || estimateTimes.isLoading()
            ? '正在載入公車路線資訊'
            : undefined
        }
      >
        <TopAppBar>
          <TopAppBarNavigation
            type="button"
            aria-label="返回"
            icon="arrow_back"
            onClick={[navigate, -1]}
          />
          <TopAppBarHeadline>{route.data()?.name ?? ''}</TopAppBarHeadline>
          <TopAppBarActions>
            <TopAppBarAction
              type="button"
              aria-label="更新到站資訊"
              icon="refresh"
              onClick={estimateTimes.refetch}
            />
          </TopAppBarActions>
        </TopAppBar>
        <Show when={route.data() !== undefined && isBidirection()}>
          <Tabs aria-label="去程／返程">
            <Tab
              id="direction-0-tab"
              aria-controls="direction-0-panel"
              variant="primary"
              active={locationHash().direction === '0'}
              onClick={[setDirection, '0']}
            >
              {`往${route.data()?.destinationStopName}`}
            </Tab>
            <Tab
              id="direction-1-tab"
              aria-controls="direction-1-panel"
              variant="primary"
              active={locationHash().direction === '1'}
              onClick={[setDirection, '1']}
            >
              {`往${route.data()?.departureStopName}`}
            </Tab>
          </Tabs>
        </Show>
      </LayoutHeader>
      <LayoutContent>
        <For each={isBidirection() ? ['0', '1'] : ['0']}>
          {(direction) => (
            <div
              id={`direction-${direction}-panel`}
              aria-labelledby={`direction-${direction}-tab`}
              role="tabpanel"
              hidden={direction !== locationHash().direction}
            >
              <List>
                <For each={stops.data()[direction as BusDirection] ?? []}>
                  {(stop) => (
                    <ListItem
                      ref={(el) => {
                        if (stop.id === locationHash().stopId) {
                          scrollToTarget(el)
                        }
                      }}
                      class={styles['item']}
                    >
                      <ItemStart as={BusArrivalTime} value={estimateTimes.data().get(stop.id)} />
                      <ItemHeadline>{stop.name}</ItemHeadline>
                      <ItemEnd
                        as={IconButton}
                        icon="bookmark"
                        aria-label={
                          bookmarkService.hasBookmark(stop.id) ? '更新收藏站牌' : '加入收藏站牌'
                        }
                        aria-expanded={stop.id === bookmarkCategories()?.stopId}
                        aria-haspopup="dialog"
                        filledIcon={bookmarkService.hasBookmark(stop.id)}
                        onClick={[openBookmarkDialog, [stop.id, stop.name]]}
                      />
                    </ListItem>
                  )}
                </For>
              </List>
            </div>
          )}
        </For>
        <BookmarkDialog
          id="bookmark-dialog"
          open={bookmarkCategories() !== null}
          categories={bookmarkCategories()?.categories ?? {}}
          onClose={onCloseBookmarkDialog}
        />
        <Snackbar
          open={snackbarText() !== undefined}
          class={styles['snackbar']}
          onClosed={popSnackbarText}
        >
          <SnackbarContent>{snackbarText()}</SnackbarContent>
        </Snackbar>
      </LayoutContent>
    </Layout>
  )
}
