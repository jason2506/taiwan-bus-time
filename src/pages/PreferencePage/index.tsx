import { useNavigate } from '@solidjs/router'
import type { JSX } from 'solid-js'
import { createSignal } from 'solid-js'

import {
  Dialog,
  DialogAction,
  DialogActions,
  DialogContent,
  DialogHeadline,
} from '@/components/Dialog'
import { Divider } from '@/components/Divider'
import { Icon } from '@/components/Icon'
import { ItemEnd, ItemHeadline, ItemStart, ItemSupportingText } from '@/components/Item'
import { Layout, LayoutContent, LayoutHeader } from '@/components/Layout'
import { List, ListItem } from '@/components/List'
import { RadioGroupDialog } from '@/components/RadioGroupDialog'
import { TopAppBar, TopAppBarHeadline, TopAppBarNavigation } from '@/components/TopAppBar'
import type { FontSize, Theme } from '@/services'
import { usePreferenceService } from '@/services'

import styles from './style.module.scss'

const commitHash = import.meta.env.COMMIT_HASH

const themeOptions: Readonly<Record<Theme, string>> = {
  system: '系統設定',
  light: '淺色模式',
  dark: '深色模式',
}

const fontSizeOptions: Readonly<Record<FontSize, string>> = {
  md: '中',
  lg: '大',
  xl: '特大',
  xxl: '超特大',
}

interface OptionGroup {
  readonly headline: string
  readonly options: Readonly<Record<string, string>>
  readonly value: string
  readonly onClose: (value: string) => void
}

export default function PreferencePage(): JSX.Element {
  const preferences = usePreferenceService()
  const [optionGroup, setOptionGroup] = createSignal<OptionGroup>()

  function openThemePreferenceDialog(): void {
    setOptionGroup({
      headline: '主題',
      options: themeOptions,
      value: preferences.get('theme'),
      onClose: (value) => {
        setOptionGroup()
        if (value !== '') {
          preferences.set('theme', value as Theme)
        }
      },
    })
  }

  function openFontSizePreferenceDialog(): void {
    setOptionGroup({
      headline: '字體大小',
      options: fontSizeOptions,
      value: preferences.get('fontSize'),
      onClose: (value) => {
        setOptionGroup()
        if (value !== '') {
          preferences.set('fontSize', value as FontSize)
        }
      },
    })
  }

  const [isPrivacyDialogOpen, setPrivacyDialogOpen] = createSignal(false)
  const navigate = useNavigate()
  return (
    <Layout>
      <LayoutHeader title="偏好設定">
        <TopAppBar>
          <TopAppBarNavigation
            type="button"
            aria-label="返回"
            icon="arrow_back"
            onClick={[navigate, -1]}
          />
          <TopAppBarHeadline>偏好設定</TopAppBarHeadline>
        </TopAppBar>
      </LayoutHeader>
      <LayoutContent>
        <List>
          <ListItem
            type="button"
            aria-expanded={optionGroup() !== undefined}
            aria-haspopup="dialog"
            onClick={openThemePreferenceDialog}
          >
            <ItemStart as={Icon} name="color_lens" />
            <ItemHeadline>主題</ItemHeadline>
            <ItemSupportingText>{themeOptions[preferences.get('theme')]}</ItemSupportingText>
          </ListItem>
          <ListItem
            type="button"
            aria-expanded={optionGroup() !== undefined}
            aria-haspopup="dialog"
            onClick={openFontSizePreferenceDialog}
          >
            <ItemStart as={Icon} name="format_size" />
            <ItemHeadline>字體大小</ItemHeadline>
            <ItemSupportingText>{fontSizeOptions[preferences.get('fontSize')]}</ItemSupportingText>
          </ListItem>
          <ListItem href="preferences/import-export">
            <ItemStart as={Icon} name="import_export" />
            <ItemHeadline>匯入／匯出</ItemHeadline>
            <ItemSupportingText>備份或還原既有的收藏站牌和偏好設定</ItemSupportingText>
          </ListItem>
        </List>
        <Divider />
        <List>
          <ListItem href="licenses">
            <ItemStart as={Icon} name="copyright" />
            <ItemHeadline>第三方授權</ItemHeadline>
            <ItemSupportingText>本服務使用的資料來源和開源專案</ItemSupportingText>
          </ListItem>
          <ListItem
            type="button"
            aria-expanded={isPrivacyDialogOpen()}
            aria-haspopup="dialog"
            onClick={[setPrivacyDialogOpen, true]}
          >
            <ItemStart as={Icon} name="privacy_tip" />
            <ItemHeadline>隱私權聲明</ItemHeadline>
            <ItemSupportingText>本服務不會蒐集任何使用者的個人資訊</ItemSupportingText>
          </ListItem>
          <ListItem
            href="https://codeberg.org/jason2506/taipei-bus-time/src/branch/main/UNLICENSE"
            target="_blank"
          >
            <ItemStart as={Icon} name="policy" />
            <ItemHeadline>授權條款</ItemHeadline>
            <ItemSupportingText>本服務將其完整原始碼貢獻至公眾領域</ItemSupportingText>
            <ItemEnd as={Icon} name="open_in_new" />
          </ListItem>
          <ListItem
            href={`https://codeberg.org/jason2506/taipei-bus-time/src/commit/${commitHash}`}
            target="_blank"
          >
            <ItemStart as={Icon} name="build" />
            <ItemHeadline>當前版本</ItemHeadline>
            <ItemSupportingText>{commitHash}</ItemSupportingText>
            <ItemEnd as={Icon} name="open_in_new" />
          </ListItem>
        </List>
        <Dialog
          open={isPrivacyDialogOpen()}
          on:closed={() => {
            setPrivacyDialogOpen(false)
          }}
        >
          <DialogHeadline>隱私權聲明</DialogHeadline>
          <DialogContent class={styles['privacy-dialog-content']}>
            <p>
              本服務拒絕置入任何廣告或追蹤程式，亦不會蒐集任何使用者的個人資訊。您使用本服務的任何個人化設置皆僅會留存於本機端。
            </p>
            <p>
              本服務託管於{' '}
              <a href="https://codeberg.page/" target="_blank">
                Codeberg Pages
                <Icon name="open_in_new" />
              </a>
              ，其會於您開啟本服務時記錄您的 IP
              位址、使用時間、使用的瀏覽器等資訊，並於七天內自動銷毀。詳情請參照該服務的
              <a href="https://codeberg.org/codeberg/org/src/PrivacyPolicy.md" target="_blank">
                隱私權聲明
                <Icon name="open_in_new" />
              </a>
              。
            </p>
          </DialogContent>
          <DialogActions>
            <DialogAction>知道了</DialogAction>
          </DialogActions>
        </Dialog>
        <RadioGroupDialog
          open={optionGroup() !== undefined}
          headline={optionGroup()?.headline ?? ''}
          options={optionGroup()?.options ?? {}}
          value={optionGroup()?.value}
          onClose={optionGroup()?.onClose}
        />
      </LayoutContent>
    </Layout>
  )
}
