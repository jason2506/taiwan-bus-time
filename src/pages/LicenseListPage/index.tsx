import { useNavigate } from '@solidjs/router'
import type { JSX } from 'solid-js'
import { For } from 'solid-js'

import { Icon } from '@/components/Icon'
import { ItemEnd, ItemHeadline, ItemSupportingText } from '@/components/Item'
import { Layout, LayoutContent, LayoutHeader } from '@/components/Layout'
import { List, ListItem } from '@/components/List'
import { TopAppBar, TopAppBarHeadline, TopAppBarNavigation } from '@/components/TopAppBar'

interface LicenseInfo {
  readonly name: string
  readonly license: string
  readonly url: string
}

const licenses: readonly LicenseInfo[] = [
  {
    name: '臺北市公共運輸處 Data.Taipei API',
    license: '政府資料開放授權條款 1.0',
    url: 'https://data.taipei/rule',
  },
  {
    name: 'Browser-FS-Access',
    license: 'Apache License 2.0',
    url: 'https://github.com/GoogleChromeLabs/browser-fs-access/blob/main/LICENSE',
  },
  {
    name: 'Classnames',
    license: 'MIT License',
    url: 'https://github.com/JedWatson/classnames/blob/main/LICENSE',
  },
  {
    name: 'Material Symbols',
    license: 'Apache License 2.0',
    url: 'https://github.com/google/material-design-icons/blob/master/LICENSE',
  },
  {
    name: 'Material Web',
    license: 'Apache License 2.0',
    url: 'https://github.com/material-components/material-web/blob/main/LICENSE',
  },
  {
    name: 'Roboto Font',
    license: 'Apache License 2.0',
    url: 'https://fonts.google.com/specimen/Roboto/about',
  },
  {
    name: 'Solid',
    license: 'MIT License',
    url: 'https://github.com/solidjs/solid/blob/main/LICENSE',
  },
  {
    name: 'Solid Primitives',
    license: 'MIT License',
    url: 'https://github.com/solidjs-community/solid-primitives/blob/main/LICENSE',
  },
  {
    name: 'Solid Router',
    license: 'MIT License',
    url: 'https://github.com/solidjs/solid-router/blob/main/LICENSE',
  },
  {
    name: 'Typia',
    license: 'MIT License',
    url: 'https://github.com/samchon/typia/blob/master/LICENSE',
  },
  {
    name: 'UUID',
    license: 'MIT License',
    url: 'https://github.com/uuidjs/uuid/blob/main/LICENSE.md',
  },
] as const

export default function LicenseListPage(): JSX.Element {
  const navigate = useNavigate()
  return (
    <Layout>
      <LayoutHeader title="第三方授權">
        <TopAppBar>
          <TopAppBarNavigation
            type="button"
            aria-label="返回"
            icon="arrow_back"
            onClick={[navigate, -1]}
          />
          <TopAppBarHeadline>第三方授權</TopAppBarHeadline>
        </TopAppBar>
      </LayoutHeader>
      <LayoutContent>
        <List>
          <For each={licenses}>
            {({ name, url, license }) => (
              <ListItem href={url} target="_blank">
                <ItemHeadline>{name}</ItemHeadline>
                <ItemSupportingText>{license}</ItemSupportingText>
                <ItemEnd as={Icon} name="open_in_new">
                  {license}
                </ItemEnd>
              </ListItem>
            )}
          </For>
        </List>
      </LayoutContent>
    </Layout>
  )
}
