import type { ContextProviderProps } from '@solid-primitives/context'
import { createContextProvider } from '@solid-primitives/context'

import { PersistentPreferenceService } from '../implementations/preference-service'
import type { PreferenceService } from '../interfaces/preference-service'

export interface PreferenceServiceContext extends PreferenceService {}

export interface PreferenceServiceProviderProps extends ContextProviderProps {}

// eslint-disable-next-line @typescript-eslint/naming-convention
const [PreferenceServiceProvider, _usePreferenceService] = createContextProvider<
  PreferenceServiceContext,
  PreferenceServiceProviderProps
>(() => new PersistentPreferenceService())

function usePreferenceService(): PreferenceServiceContext {
  const context = _usePreferenceService()
  if (context === undefined) {
    throw new Error('PreferenceService is not provided')
  }

  return context
}

export { PreferenceServiceProvider, usePreferenceService }
