import type { Accessor } from 'solid-js'

import type { AsyncData } from './async-data'

export type BusDirection = '0' | '1'

export interface BusStop {
  readonly id: string
  readonly routeId: string
  readonly name: string
  readonly direction: BusDirection
}

export interface BusRoute {
  readonly id: string
  readonly name: string
  readonly cityName: string
  readonly departureStopName: string
  readonly destinationStopName: string
}

export interface BusInfoService {
  readonly getRoutes: () => AsyncData<readonly BusRoute[]>
  readonly getRoutesByIds: (
    routeIds: Accessor<IterableIterator<string>>,
  ) => AsyncData<ReadonlyMap<string, BusRoute>>
  readonly getRoute: (routeId: Accessor<string>) => AsyncData<BusRoute | undefined>
  readonly getStopsByRoute: (
    routeId: Accessor<string>,
  ) => AsyncData<[readonly BusStop[], readonly BusStop[]]>
  readonly getStopsByIds: (
    stopIds: Accessor<IterableIterator<string>>,
  ) => AsyncData<ReadonlyMap<string, BusStop>>
  readonly getStop: (stopId: Accessor<string>) => AsyncData<BusStop | undefined>
  readonly getEstimateTimesByRoute: (
    routeId: Accessor<string>,
  ) => AsyncData<ReadonlyMap<string, number>>
  readonly getEstimateTimesByStops: (
    stopIds: Accessor<IterableIterator<string>>,
  ) => AsyncData<ReadonlyMap<string, number>>
}
