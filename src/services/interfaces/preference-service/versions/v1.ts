export type Theme = 'system' | 'dark' | 'light'

export interface BusStopBookmarkData extends Readonly<Record<string, readonly string[]>> {}

export interface Preference {
  readonly theme: Theme
  readonly bookmarkData: BusStopBookmarkData
}
