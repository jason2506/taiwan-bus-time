import type { Accessor } from 'solid-js'

export interface AsyncData<T> {
  readonly isLoading: Accessor<boolean>
  readonly data: Accessor<T>
  readonly error: Accessor<unknown>
  readonly refetch: () => void
}
