import type { Accessor, Setter } from 'solid-js'
import { createEffect, createSignal } from 'solid-js'
import { createAssertParse, createStringify } from 'typia/lib/json'

import type { Preference, PreferenceService } from '../../interfaces/preference-service'
import { defaultPreference, parse, version } from './versions'

const localStorageKey = 'taipei-bus-time'

interface PreferenceData {
  readonly version: string | number
  readonly preferences: unknown
}

const assertParsePreferenceData = createAssertParse<PreferenceData>()
const stringifyPreferenceData = createStringify<PreferenceData>()

function parsePreferenceJson(json: string): Preference {
  const { version, preferences } = assertParsePreferenceData(json)
  return parse(preferences, `${version}`)
}

export class PersistentPreferenceService implements PreferenceService {
  private readonly _data: Accessor<Preference>
  private readonly _setData: Setter<Preference>

  public constructor() {
    let preference = defaultPreference
    const json = localStorage.getItem(localStorageKey)
    if (json !== null) {
      try {
        preference = parsePreferenceJson(json)
      } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error)
      }
    }

    const [data, setData] = createSignal<Preference>(preference)
    this._data = data
    this._setData = setData
    createEffect(() => {
      localStorage.setItem(localStorageKey, this.export())
    })
  }

  public get<P extends keyof Preference>(name: P): Preference[P] {
    return this._data()[name]
  }

  public set<P extends keyof Preference>(name: P, value: Preference[P]): void {
    this._setData({
      ...this._data(),
      [name]: value,
    })
  }

  public export(): string {
    return stringifyPreferenceData({
      version,
      preferences: this._data(),
    })
  }

  public import(json: string): void {
    this._setData({
      ...defaultPreference,
      ...parsePreferenceJson(json),
    })
  }
}
