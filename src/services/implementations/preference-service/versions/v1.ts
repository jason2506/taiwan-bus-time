import { createAssert } from 'typia'

import type { v1 } from '../../../interfaces/preference-service'

const defaultPreference: v1.Preference = {
  theme: 'system',
  bookmarkData: {},
}

const assertPreference = createAssert<Partial<v1.Preference>>()

export function parse(data: unknown, version: string): v1.Preference {
  if (version !== '1') {
    throw new Error(`Unknown preference version: ${version}`)
  }

  const preferences = assertPreference(data)
  return {
    ...defaultPreference,

    // only remain known preferences
    ...Object.fromEntries(
      Object.entries(preferences).filter(([name]) => name in defaultPreference),
    ),
  }
}
