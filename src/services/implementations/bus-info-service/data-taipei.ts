import type { Accessor, InitializedResourceReturn, Resource } from 'solid-js'
import { createMemo, createResource } from 'solid-js'

import type { AsyncData } from '../../interfaces/async-data'
import type { BusInfoService, BusRoute, BusStop } from '../../interfaces/bus-info-service'

const cityIds = ['TPE', 'NWT'] as const

type CityId = (typeof cityIds)[number]

const cityDataPaths: Readonly<Record<CityId, string>> = {
  TPE: 'blobbus',
  NWT: 'ntpcbus',
}

const cityNames: Readonly<Record<CityId, string>> = {
  TPE: '臺北市',
  NWT: '新北市',
}

interface DataTaipeiBusStop {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  readonly Id: number
  readonly routeId: number
  readonly nameZh: string
  readonly seqNo: number
  readonly goBack: '0' | '1' | '2'
}

interface DataTaipeiBusRoute {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  readonly Id: number
  readonly nameZh: string
  readonly departureZh: string
  readonly destinationZh: string
}

interface DataTaipeiBusEstimateTime {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  readonly StopID: number
  // eslint-disable-next-line @typescript-eslint/naming-convention
  readonly EstimateTime: string
}

async function steamToJson<T>(steam: ReadableStream | undefined): Promise<T> {
  const response = new Response(steam)
  return (await response.json()) as T
}

async function fetchJsonGz<T>(cityId: CityId, name: string): Promise<T> {
  const response = await fetch(
    `https://tcgbusfs.blob.core.windows.net/${cityDataPaths[cityId]}/Get${name}.gz`,
  )
  const ds = new DecompressionStream('gzip')
  const steam = response.body?.pipeThrough(ds)
  return await steamToJson<T>(steam)
}

interface StopWithSeqNo {
  readonly id: string
  readonly seqNo: number
}

function compareStops(a: StopWithSeqNo, b: StopWithSeqNo): number {
  return a.seqNo > b.seqNo ? 1 : a.seqNo < b.seqNo ? -1 : 0
}

function getOrderedStopIds(stops: StopWithSeqNo[]): readonly string[] {
  return stops.sort(compareStops).map(({ id }) => id)
}

async function fetchStopsByCity(
  cityId: CityId,
  stops: Map<string, BusStop>,
  stopsOfRoutes: Map<string, readonly [readonly string[], readonly string[]]>,
): Promise<void> {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  const json = await fetchJsonGz<{ readonly BusInfo: readonly DataTaipeiBusStop[] }>(cityId, 'Stop')
  const unorderedStopsOfRoutes = new Map<string, [StopWithSeqNo[], StopWithSeqNo[]]>()
  for (const stopData of json.BusInfo) {
    const stopId = `${cityId}${stopData.Id}`
    const direction = stopData.goBack
    if (direction !== '0' && direction !== '1') {
      // eslint-disable-next-line no-console
      console.warn(`Stop info with unknown direction: ${stopId}`)
      continue
    }

    const routeId = `${cityId}${stopData.routeId}`
    stops.set(stopId, {
      id: stopId,
      routeId,
      name: stopData.nameZh,
      direction,
    })

    let stopsOfRoute = unorderedStopsOfRoutes.get(routeId)
    if (stopsOfRoute === undefined) {
      stopsOfRoute = [[], []]
      unorderedStopsOfRoutes.set(routeId, stopsOfRoute)
    }

    stopsOfRoute[direction === '0' ? 0 : 1].push({
      id: stopId,
      seqNo: stopData.seqNo,
    })
  }

  for (const [routeId, stops] of unorderedStopsOfRoutes) {
    stopsOfRoutes.set(routeId, [getOrderedStopIds(stops[0]), getOrderedStopIds(stops[1])])
  }
}

async function fetchStops(): Promise<
  readonly [
    ReadonlyMap<string, BusStop>,
    ReadonlyMap<string, readonly [readonly string[], readonly string[]]>,
  ]
> {
  const stops = new Map<string, BusStop>()
  const stopsOfRoutes = new Map<string, readonly [readonly string[], readonly string[]]>()
  await Promise.all(
    cityIds.map(async (cityId) => {
      await fetchStopsByCity(cityId, stops, stopsOfRoutes)
    }),
  )
  return [stops, stopsOfRoutes]
}

async function fetchRoutesByCity(cityId: CityId, routes: Map<string, BusRoute>): Promise<void> {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  const json = await fetchJsonGz<{ readonly BusInfo: readonly DataTaipeiBusRoute[] }>(
    cityId,
    'Route',
  )
  for (const routeData of json.BusInfo) {
    const routeId = `${cityId}${routeData.Id}`
    routes.set(routeId, {
      id: routeId,
      name: routeData.nameZh,
      cityName: cityNames[cityId],
      departureStopName: routeData.departureZh,
      destinationStopName: routeData.destinationZh,
    })
  }
}

async function fetchRoutes(): Promise<ReadonlyMap<string, BusRoute>> {
  const routes = new Map<string, BusRoute>()
  await Promise.all(
    cityIds.map(async (cityId) => {
      await fetchRoutesByCity(cityId, routes)
    }),
  )
  return new Map(
    [...routes.entries()].sort(([, { name: a }], [, { name: b }]) => (a > b ? 1 : a < b ? -1 : 0)),
  )
}

async function fetchEstimateTimesByCity(cityId: CityId): Promise<ReadonlyMap<string, number>> {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  const json = await fetchJsonGz<{ readonly BusInfo: readonly DataTaipeiBusEstimateTime[] }>(
    cityId,
    'EstimateTime',
  )

  const estimateTimes = new Map<string, number>()
  for (const estimateTimeData of json.BusInfo) {
    const stopId = `${cityId}${estimateTimeData.StopID}`
    estimateTimes.set(stopId, Number.parseInt(estimateTimeData.EstimateTime))
  }

  return estimateTimes
}

function getCityId(id: string): CityId | undefined {
  const cityId = id.substring(0, 3)
  const isCityId = (cityIds as readonly string[]).includes(cityId)
  return isCityId ? (cityId as CityId) : undefined
}

export class DataTaipeiBusInfoService implements BusInfoService {
  private readonly _stops: AsyncData<ReadonlyMap<string, BusStop>>
  private readonly _routes: AsyncData<ReadonlyMap<string, BusRoute>>
  private readonly _stopsOfRoutes: AsyncData<
    ReadonlyMap<string, readonly [readonly string[], readonly string[]]>
  >

  private readonly _estimateTimesResources: Readonly<
    Record<CityId, InitializedResourceReturn<ReadonlyMap<string, number>>>
  >

  public constructor() {
    const [stops, { refetch: refetchStops }] = createResource(fetchStops, {
      initialValue: [new Map(), new Map()],
    })
    this._stops = {
      isLoading: () => stops.loading,
      data: () => (stops.error === undefined ? stops()[0] : new Map()),
      error: () => stops.error as unknown,
      refetch: () => {
        void refetchStops()
      },
    }
    this._stopsOfRoutes = {
      isLoading: () => stops.loading,
      data: () => (stops.error === undefined ? stops()[1] : new Map()),
      error: () => stops.error as unknown,
      refetch: () => {
        void refetchStops()
      },
    }

    const [routes, { refetch: refetchRoutes }] = createResource(fetchRoutes, {
      initialValue: new Map(),
    })
    this._routes = {
      isLoading: () => routes.loading,
      data: () => (routes.error === undefined ? routes() : new Map()),
      error: () => routes.error as unknown,
      refetch: () => {
        void refetchRoutes()
      },
    }

    this._estimateTimesResources = Object.fromEntries(
      cityIds.map((cityId) => [
        cityId,
        // eslint-disable-next-line @typescript-eslint/promise-function-async
        createResource(() => fetchEstimateTimesByCity(cityId), {
          initialValue: new Map(),
        }),
      ]),
    ) as Record<CityId, InitializedResourceReturn<ReadonlyMap<string, number>>>
  }

  public getRoutes(): AsyncData<readonly BusRoute[]> {
    const { isLoading, data, error, refetch } = this._routes
    return {
      isLoading,
      data: () => Array.from(data().values()),
      error,
      refetch,
    }
  }

  public getRoutesByIds(
    routeIds: Accessor<IterableIterator<string>>,
  ): AsyncData<ReadonlyMap<string, BusRoute>> {
    const { isLoading, data, error, refetch } = this._routes
    const routesByIdsData = createMemo(() => {
      const routes = data()
      const routesByIds = new Map<string, BusRoute>()
      for (const routeId of routeIds()) {
        const route = routes.get(routeId)
        if (route !== undefined) {
          routesByIds.set(routeId, route)
        }
      }

      return routesByIds
    })

    return {
      isLoading,
      data: routesByIdsData,
      error,
      refetch,
    }
  }

  public getRoute(routeId: Accessor<string>): AsyncData<BusRoute | undefined> {
    const { isLoading, data, error, refetch } = this._routes
    return {
      isLoading,
      data: () => data().get(routeId()),
      error,
      refetch,
    }
  }

  public getStopsByRoute(
    routeId: Accessor<string>,
  ): AsyncData<[readonly BusStop[], readonly BusStop[]]> {
    const stopsOfRoutes = this._stopsOfRoutes
    const stops = this._stops
    const stopsByRouteData = createMemo(() => {
      const stopsOfRoute = stopsOfRoutes.data().get(routeId())
      const stopsByRoute: [BusStop[], BusStop[]] = [[], []]
      if (stopsOfRoute === undefined) {
        return stopsByRoute
      }

      const stopsData = stops.data()
      for (const [direction, stopIds] of stopsOfRoute.entries()) {
        const stopsOfRoute = stopsByRoute[direction as 0 | 1]
        for (const stopId of stopIds) {
          const stop = stopsData.get(stopId)
          if (stop === undefined) {
            throw new Error(`Unknown bus stop ID ${stopId} in route ${routeId()}`)
          }

          stopsOfRoute.push(stop)
        }
      }

      return stopsByRoute
    })

    return {
      isLoading: () => stopsOfRoutes.isLoading() || stops.isLoading(),
      data: stopsByRouteData,
      error: () => stopsOfRoutes.error() ?? stops.error(),
      refetch: () => {
        stopsOfRoutes.refetch()
        stops.refetch()
      },
    }
  }

  public getStopsByIds(
    stopIds: Accessor<IterableIterator<string>>,
  ): AsyncData<ReadonlyMap<string, BusStop>> {
    const { isLoading, data, error, refetch } = this._stops
    const stopsByIdsData = createMemo(() => {
      const stops = data()
      const stopsByIds = new Map<string, BusStop>()
      for (const stopId of stopIds()) {
        const stop = stops.get(stopId)
        if (stop !== undefined) {
          stopsByIds.set(stopId, stop)
        }
      }

      return stopsByIds
    })

    return {
      isLoading,
      data: stopsByIdsData,
      error,
      refetch,
    }
  }

  public getStop(stopId: Accessor<string>): AsyncData<BusStop | undefined> {
    const { isLoading, data, error, refetch } = this._stops
    return {
      isLoading,
      data: () => data().get(stopId()),
      error,
      refetch,
    }
  }

  public getEstimateTimesByRoute(
    routeId: Accessor<string>,
  ): AsyncData<ReadonlyMap<string, number>> {
    const stopsOfRoutes = this._stopsOfRoutes
    return this._getEstimateTimes(() => {
      const cityId = getCityId(routeId())
      if (cityId === undefined) {
        throw new Error(`Can not detect city by route ID: ${routeId()}`)
      }

      const stopsOfRoute = stopsOfRoutes.data().get(routeId()) ?? [[], []]
      return [[cityId as CityId], stopsOfRoute.flat()]
    })
  }

  public getEstimateTimesByStops(
    stopIds: Accessor<IterableIterator<string>>,
  ): AsyncData<ReadonlyMap<string, number>> {
    return this._getEstimateTimes(() => {
      const cityIdSet = new Set<CityId>()
      const stopIdSet = new Set(stopIds())
      for (const stopId of stopIdSet) {
        const cityId = getCityId(stopId)
        if (cityId === undefined) {
          throw new Error(`Can not detect city by stop ID: ${stopId}`)
        }

        cityIdSet.add(cityId as CityId)
      }

      return [Array.from(cityIdSet), Array.from(stopIdSet)]
    })
  }

  private _getEstimateTimes(
    getIds: Accessor<[readonly CityId[], readonly string[]]>,
  ): AsyncData<ReadonlyMap<string, number>> {
    const estimateTimesResourceReturns = this._estimateTimesResources
    const ids = createMemo(() => getIds())
    const memo = createMemo(() => {
      const [cityIds, stopIds] = ids()
      const estimateTimesResources: Array<Resource<ReadonlyMap<string, number>>> = []
      for (const cityId of cityIds) {
        const [resource] = estimateTimesResourceReturns[cityId]
        estimateTimesResources.push(resource)
      }

      const isLoading = estimateTimesResources.reduce(
        (isLoading, estimateTimes) => isLoading || estimateTimes.loading,
        false,
      )

      const error = estimateTimesResources.reduce<unknown>(
        (error: unknown, estimateTimes) => error ?? (estimateTimes.error as unknown),
        // eslint-disable-next-line unicorn/no-useless-undefined
        undefined,
      )

      if (error !== undefined) {
        return {
          isLoading,
          data: new Map(),
          error,
        }
      }

      const estimateTimes = new Map<string, number>()
      for (const stopId of stopIds) {
        const cityId = getCityId(stopId)
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const [resource] = estimateTimesResourceReturns[cityId!]
        const estimateTime = resource().get(stopId)
        if (estimateTime !== undefined) {
          estimateTimes.set(stopId, estimateTime)
        }
      }

      return {
        isLoading,
        data: estimateTimes,
        error,
      }
    })

    function refetch(): void {
      const [cityIds] = ids()
      for (const cityId of cityIds) {
        const [_, { refetch }] = estimateTimesResourceReturns[cityId]
        void refetch()
      }
    }

    // eslint-disable-next-line solid/reactivity
    refetch()

    return {
      isLoading: () => memo().isLoading,
      data: () => memo().data,
      error: () => memo().error,
      refetch,
    }
  }
}
