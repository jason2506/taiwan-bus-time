import type { Accessor } from 'solid-js'
import { createMemo } from 'solid-js'
import { v4 as uuid } from 'uuid'

import type { AsyncData } from '../interfaces/async-data'
import type { BusInfoService } from '../interfaces/bus-info-service'
import type {
  BusStopBookmark,
  BusStopBookmarkService,
  BusStopCategory,
} from '../interfaces/bus-stop-bookmark-service'
import type { PreferenceService } from '../interfaces/preference-service'

export class ReactiveBusStopBookmarkService implements BusStopBookmarkService {
  private readonly _preferenceService: PreferenceService
  private readonly _busInfoService: BusInfoService
  private readonly _bookmarks: Accessor<
    ReadonlyMap<
      string,
      {
        readonly name: string
        readonly stopIds: ReadonlySet<string>
      }
    >
  >

  public constructor(preferenceService: PreferenceService, busInfoService: BusInfoService) {
    this._preferenceService = preferenceService
    this._busInfoService = busInfoService
    this._bookmarks = createMemo(() => {
      const bookmarkData = preferenceService.get('bookmarks')
      const bookmarkList: Array<[string, { name: string; stopIds: Set<string> }]> = []
      for (const [id, { name, stopIds }] of Object.entries(bookmarkData)) {
        if (stopIds.length > 0) {
          bookmarkList.push([id, { name, stopIds: new Set(stopIds) }])
        }
      }

      return new Map(bookmarkList)
    })
  }

  public hasBookmark(stopId: string): boolean {
    for (const { stopIds } of this._bookmarks().values()) {
      if (stopIds.has(stopId)) {
        return true
      }
    }

    return false
  }

  public getStopIds(): ReadonlySet<string> {
    const stopIdSet = new Set<string>()
    for (const { stopIds } of this._bookmarks().values()) {
      for (const stopId of stopIds) {
        stopIdSet.add(stopId)
      }
    }

    return stopIdSet
  }

  public getBookmarks(): AsyncData<ReadonlyMap<string, BusStopCategory>> {
    const busInfoService = this._busInfoService
    const stops = busInfoService.getStopsByIds(() => this.getStopIds().values())
    const routes = busInfoService.getRoutesByIds(() => {
      const stopsData = stops.data()
      const routeIdSet = new Set<string>()
      for (const stop of stopsData.values()) {
        routeIdSet.add(stop.routeId)
      }

      return routeIdSet.values()
    })

    const bookmarksData = createMemo<Map<string, BusStopCategory>>((prevData) => {
      if (
        stops.isLoading() ||
        stops.error() !== undefined ||
        routes.isLoading() ||
        routes.error() !== undefined
      ) {
        return prevData ?? new Map<string, BusStopCategory>()
      }

      const stopsData = stops.data()
      const routesData = routes.data()
      const bookmarks = new Map<string, BusStopCategory>()
      for (const [categoryId, { name, stopIds }] of this._bookmarks()) {
        const bookmarksByCategory: BusStopBookmark[] = []
        for (const stopId of stopIds) {
          const stop = stopsData.get(stopId)
          if (stop === undefined) {
            throw new Error(`Stop not found: ${stopId}`)
          }

          const route = routesData.get(stop.routeId)
          if (route === undefined) {
            throw new Error(`Route not found: ${stop.routeId}`)
          }

          bookmarksByCategory.push({
            stopId,
            stopName: stop.name,
            direction: stop.direction,
            routeId: route.id,
            routeName: route.name,
            destinationStopName:
              stop.direction === '0' ? route.destinationStopName : route.departureStopName,
          })
        }

        bookmarks.set(categoryId, {
          name,
          bookmarks: bookmarksByCategory,
        })
      }

      return bookmarks
    })

    return {
      isLoading: () => stops.isLoading() || routes.isLoading(),
      data: bookmarksData,
      error: () => stops.error() ?? routes.error(),
      refetch: () => {
        stops.refetch()
        routes.refetch()
      },
    }
  }

  public getCategories(): Readonly<Record<string, string>> {
    const categories: Record<string, string> = {}
    for (const [categoryId, category] of this._bookmarks()) {
      categories[categoryId] = category.name
    }

    return categories
  }

  public getCategoriesByStop(stopId: string): readonly string[] {
    const result: string[] = []
    for (const [categoryId, { stopIds }] of this._bookmarks()) {
      if (stopIds.has(stopId)) {
        result.push(categoryId)
      }
    }

    return result
  }

  public renameCategory(categoryId: string, name: string): void {
    const preferenceService = this._preferenceService
    const bookmarkData = preferenceService.get('bookmarks')
    const category = bookmarkData[categoryId]
    if (category === undefined) {
      throw new Error(`Category not found: ${categoryId}`)
    }

    preferenceService.set('bookmarks', {
      ...bookmarkData,
      [categoryId]: {
        ...category,
        name,
      },
    })
  }

  public reorderCategory(from: number, to: number): void {
    const preferenceService = this._preferenceService
    const bookmarkData = preferenceService.get('bookmarks')
    const entries = Object.entries(bookmarkData)
    const entry = entries[from]
    if (entry === undefined) {
      throw new Error(`Category index out of range: ${from}`)
    }

    entries.splice(from, 1)
    entries.splice(to, 0, entry)
    preferenceService.set('bookmarks', Object.fromEntries(entries))
  }

  public reorderBookmark(categoryId: string, from: number, to: number): void {
    if (from === to) {
      return
    }

    const preferenceService = this._preferenceService
    const bookmarkData = preferenceService.get('bookmarks')

    const category = bookmarkData[categoryId]
    if (category === undefined) {
      throw new Error(`Bookmark category not found: ${category}`)
    }

    const stopIds = [...category.stopIds]
    const stopId = stopIds[from]
    if (stopId === undefined) {
      throw new Error(`Bookmark at index not found: ${from}`)
    }

    stopIds.splice(from, 1)
    stopIds.splice(to, 0, stopId)

    preferenceService.set('bookmarks', {
      ...bookmarkData,
      [categoryId]: {
        ...category,
        stopIds,
      },
    })
  }

  public updateBookmarks(
    stopIds: readonly string[],
    updatedCategories: Readonly<Record<string, boolean>>,
    newCategories: readonly string[],
  ): void {
    const preferenceService = this._preferenceService
    const bookmarkData = {
      ...preferenceService.get('bookmarks'),
    }

    for (const [categoryId, selected] of Object.entries(updatedCategories)) {
      const category = bookmarkData[categoryId]
      if (category === undefined) {
        continue
      }

      const stopIdSet = new Set(category.stopIds)
      for (const stopId of stopIds) {
        if (selected) {
          stopIdSet.add(stopId)
        } else {
          stopIdSet.delete(stopId)
        }
      }

      if (stopIdSet.size > 0) {
        bookmarkData[categoryId] = {
          ...category,
          stopIds: Array.from(stopIdSet.values()),
        }
      } else {
        // eslint-disable-next-line @typescript-eslint/no-dynamic-delete
        delete bookmarkData[categoryId]
      }
    }

    for (const category of newCategories) {
      let id: string
      do {
        id = uuid()
      } while (id in bookmarkData)
      bookmarkData[id] = {
        name: category,
        stopIds,
      }
    }

    preferenceService.set('bookmarks', bookmarkData)
  }
}
